<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends ApiController
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $levels = Task::all();
        return  $this->showList($levels) ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'task' => 'required|min:10|unique:tasks,task',

        ];

        $this->validate($request,$rules);
        $task = Task::create([
            "task" => $request->task,
        ]);

        return $this->successResponse("Tarea creada",200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */

    public function complete(Request $request, Task $task)
    {
        //
        if(!Task::exists($task->id)){
            return $this->errorResponse("Tarea no existe",400);
        }
        $task->status  = true;
        $task->save();
        return $this->successResponse("Tarea cambió a completada",200);

    }

    public function pending(Request $request, Task $task)
    {
        if(!Task::exists($task->id)){
            return $this->errorResponse("Tarea no existe",400);
        }
        $task->status  = true;
        $task->save();

        return $this->successResponse("Tarea cambió a pendiente",200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
        if(!Task::exists($task->id)){
            return $this->errorResponse("Tarea no existe",400);
        }

        $task->delete();
        return $this->successResponse("Tarea eliminada",200);
    }

    public function destroyAll(Request $request)
    {
        //
        Task::query()->delete();
        return $this->successResponse("Tareas eliminadas",200);
    }
}
