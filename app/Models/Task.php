<?php

namespace App\Models;

use App\Transformers\TaskTransform;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{

    use HasFactory;
    use SoftDeletes;
    public $transformer = TaskTransform::class;

    protected $table = 'tasks';
    protected $dates = ['created_at','update_at','deleted_at'];
    protected $fillable = [
        'id',
        'task',
        'status',
    ];
}
