<?php

namespace App\Traits;

use App\Transformers\TaskTransform;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Fractal;
trait ApiResponse{

    protected function successResponse($data,$code){
        return response()->json($data,$code);
    }

    protected function showList( $collection,$code= 200){
        if($collection->first() == null){
            return  $this->successResponse(['data'=>$collection],$code);
        };
        if(!$collection->first()){
            return  $this->successResponse(['data'=>$collection],$code);

        }

        $transformer = $collection->first()->transformer;
        $collection = $this->transformData($collection,$transformer);

        return $this->successResponse(['data'=>$collection],$code);
    }


    protected function showAll( $collection,$code= 200){

        if(!$collection->first()){
            return  $this->successResponse(['data'=>$collection],$code);
        }
        if(isset($collection->first()->transformer) == false){
            return $this->successResponse(['data'=>$collection],$code);
        }

        $transformer = $collection->first()->transformer;
        $collection = $this->filterData($collection,$transformer);
        if ($collection instanceof \Illuminate\Support\Collection){
            $collection = $this->paginate($collection);
        }else{
            $collection = $collection->paginate(10);
        }
        $collection = $this->transformData($collection,$transformer);
        return $this->successResponse(['data'=>$collection],$code);
    }

    protected function showRelationshipPaginate($collection,$transformer,$code= 200){

        try {

            $collection = $this->filterData($collection,$transformer);
            //if ($collection instanceof \Illuminate\Pagination\LengthAwarePaginator){}
            $collection = $collection->paginate(10);
            $collection = $this->transformData($collection,$transformer);

            return $this->successResponse(['data'=>$collection],$code);
        }
        catch (Exception $e){
            return $this->successResponse(['data'=>[]],$code);
        }

    }

    protected function showOne(Model $instance = null,$code= 200){
        if ($instance!=null){
            $transformer = $instance->transformer;
            $instance = $this->transformData($instance,$transformer);
            return $this->successResponse($instance,$code);
        }else{
            return $this->successResponse($instance,$code);
        }
    }

    protected function errorResponse($message,$code){
        return response()->json(['message'=> $message,'code'=>$code],$code);
    }

    protected function transformData($data,$tranformer){
        // $transformation = fractal($data,new $tranformer);
        // return $transformation->toArray();

       return Fractal::collection($data)->transformWith(new $tranformer)->toArray();


    }
    //ordena las colecciones por attribute que se le mande
    protected function sortData( $collection,$transformer ){
        if(request()->has('sort_by')){
            $attribute = $transformer::originalAttributes(request()->sort_by);
            $collection = $collection->sort_by->{$attribute};
        }
        return $collection;
    }

    protected function paginate(Collection $collection){
        //permitiendo el paginado personalizado
        $rules = [
            'per_page' => 'integer|min:2|max:50'
        ];
        Validator::validate(request()->all(),$rules);
        $page = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 10;

        if(request()->has('per_page')){
            $perPage = (int)request()->per_page;
        }
        $result = $collection->slice(($page -1) * $perPage,$perPage)->values();
        $paginate  = new LengthAwarePaginator($result,$collection->count(),$perPage,$page,['path'=>LengthAwarePaginator::resolveCurrentPath()]);
        $paginate->appends(request()->all());
        return $paginate;
    }

    protected function filterData( $collection,$transformer ){

        foreach(request()->query() as  $query => $value){
                $attribute = $transformer::originalAttributes($query);
                if(isset($attribute,$value)){
                   $collection = $collection->where($attribute, 'ilike', '%'.$value.'%');;
                }

        }

        return $collection;
    }
}
