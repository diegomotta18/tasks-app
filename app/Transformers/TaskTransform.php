<?php

namespace App\Transformers;


use App\Models\Task;
use League\Fractal\TransformerAbstract;

class TaskTransform extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Task $task)
    {
        return [
            //
            'id' => (int)$task->id,
            'tarea' => (string) $task->task,
            'estado' => (bool) $task->status,
            'fechaCreacion' => (string)$task->created_at,
            'fechaActualizacion' => (string)$task->updated_at,
            'fechaEliminacion' => isset($task->deleted_at) ? (string) $task->deleted_at : null
        ];
    }

    public static function originalAttributes($index){
        $attributes = [
            //
            'id' => 'id',
            'task' => 'tarea',
            'status' => 'estado',
            'created_at' =>'fechaCreacion',
            'updated_at' =>'fechaActualizacion',
            'deletd_at' => 'fechaEliminacion'
        ];

        return isset($attributes[$index]) ? $attributes[$index]:null;
    }

    public static function transformedAttribute($index){

        $attributes = [
            'id'                 => 'id',
            'tarea'              => 'task',
            'estado'             => 'status',
            'fechaCreacion'      => 'created_at',
            'fechaActualizacion' =>'updated_at',
            'fechaEliminacion'   =>'deletd_at',
        ];

        return $attributes[$index] ?? null;
    }
}
