<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::put('tasks/{task}/complete', [\App\Http\Controllers\TaskController::class, 'complete']);
Route::put('tasks/{task}/pending', [\App\Http\Controllers\TaskController::class, 'pending']);
Route::post('tasks/destroyAll', [\App\Http\Controllers\TaskController::class, 'destroyAll']);
Route::resource('tasks',\App\Http\Controllers\TaskController::class,['only'=>['index','store','destroy']]);